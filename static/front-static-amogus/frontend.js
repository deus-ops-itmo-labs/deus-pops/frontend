window.addEventListener("load", function(event) {
    let papa = document.querySelector("#papa")
    fetch('/api'+window.location.pathname).then((response) => response.json().then((text) => {
        papa.innerHTML = JSON.stringify(text, null, 2)
    }))
});