FROM nginx:latest
COPY ./static /var/www/html
COPY ./nginx/conf.d/nginx.conf /etc/nginx/nginx.conf